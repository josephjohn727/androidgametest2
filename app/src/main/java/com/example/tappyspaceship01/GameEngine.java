package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    Player player;
    Item candy;

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int lives = 3;
    int score = 0;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();
        // put the initial starting position of your dinasaur and items
        this.player = new Player(getContext(), 1500, 400);
        this.candy = new Item(getContext(), 75,150);

    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }



    private void spawnPlayer() {

        //@TODO: Start the player at the right side of screen

    }

    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

        // @TODO: Update position of player based on mouse tap
        if (this.fingerAction == "down") {

            player.setyPosition(player.getyPosition() + 180);
            player.updateHitbox();
        }
        else if (this.fingerAction == "up") {
            player.setyPosition(player.getyPosition() - 180);
            player.updateHitbox();
        }

        // candy moves right
        this.candy.setxPosition(this.candy.getxPosition() +25);

        // MOVE THE HITBOX (recalcluate the position of the hitbox)
        this.candy.updateHitbox();

        if (this.player.getHitbox().intersect(this.candy.getHitbox()) == true) {
            // the dinosaur and candy are colliding
            score = score +1;

        }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);

            // draw the lanes on the screen
            canvas.drawRect(25,180,1300,220,paintbrush);
            canvas.drawRect(25,360,1300,400,paintbrush);
            canvas.drawRect(25,540,1300,580,paintbrush);
            canvas.drawRect(25,720,1300,760,paintbrush);

            // draw the dinosaur
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);

            // draw the candy
            canvas.drawBitmap(candy.getImage(), candy.getxPosition(), candy.getyPosition(), paintbrush);
            // 2. draw the enemy's hitbox
            canvas.drawRect(candy.getHitbox(), paintbrush);

            // DRAW GAME STATS
            // -----------------------------

            // lives
            paintbrush.setColor(Color.YELLOW);
            paintbrush.setTextSize(60);
            canvas.drawText("LIVES: " + lives,
                    1300,
                    120,
                    paintbrush
            );


            canvas.drawText("SCORE: " + score,
                    1500,
                    120,
                    paintbrush
            );







            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        // ---------------------------------------------------------
        // Get position of the tap
        // Compare position of tap to the middle of the screen
        // If tap is on up, move dinosaur up
        // If tap is on down, move dinosaur down

        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            // 1. Get position of tap
            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();
            Log.d(TAG, "Person's pressed: "
                    + fingerXPosition + ","
                    + fingerYPosition);


            // 2. Compare position of tap to middle of screen
            int middleOfScreen = this.screenHeight / 2;
            if (fingerXPosition <= middleOfScreen) {
                // 3. If tap is on up, dinosaur  go up
                fingerAction = "up";
            }
            else if (fingerXPosition > middleOfScreen) {
                // 4. If tap is on down, racket should go down
                fingerAction = "down";
            }
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
        }
        return true;
    }
}
